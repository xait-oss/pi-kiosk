raspberrypi_kiosk
=========

Setup a raspberry pi in kiosk mode for a page on a supplied url.


Requirements
------------

It might be necessary to manually run `sudo apt update` on the PIs if they have been unused for a long time.

Role Variables
--------------

`page` is the url of the `meetingroom_schedule_server` with a roomname path.

`usb_disabled` is an optional variable to set to true if you want to disable usb ports on the pi.
If `usb_disabled` is false or omitted, the usb ports are by default set back to enabled.
`idVendor` and `idProduct` are attributes needed for the ethernet port to disable all other usb ports.
They seem to be default for a same model of Pi. The defaults are 
`idVendor: 0424` and `idProduct: 7800` for the Pi 4 models used for testing.

Finding `idVendor` and `idProduct`:
```sh
$ lsusb -t
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=dwc_otg/1p, 480M
    |__ Port 1: Dev 2, If 0, Class=Hub, Driver=hub/4p, 480M
        |__ Port 1: Dev 3, If 0, Class=Hub, Driver=hub/3p, 480M
            |__ Port 1: Dev 4, If 0, Class=Vendor Specific Class, Driver=lan78xx, 480M

# We see that ethernet driver lan78xx is on bus 1, port 1.1.1, which is a series of hubs till the last port
# We look at info about the device. We are interested in the first entry only, so I've cut of excess output.

$ udevadm info -a /sys/bus/usb/devices/1-1.1.1

...

  looking at device '/devices/platform/soc/3f980000.usb/usb1/1-1/1-1.1/1-1.1.1':
    ...
    ATTR{idProduct}=="7800"
    ...
    ATTR{idVendor}=="0424"
    ...
```

Example Playbook
----------------

```yaml
- hosts: all
  roles:
  - role: raspberrypi_kiosk
    vars:
      page: "page.organization.com/rooms/<roomname>"
      usb_disabled: false  # true if you want to disable access to usb ports
```

The `page` variable can also be set per raspberrypi in the inventory.
```ini
# Inventory.ini
meetingroom_1 ansible_ssh_host=192.168.10.1 page="page.organization.com/rooms/meetingroom_1"
meetingroom_2 ansible_ssh_host=192.168.10.2 page="page.organization.com/rooms/meetingroom_2"
```

License
-------

GPL-3.0-or-later
